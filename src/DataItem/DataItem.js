import React from 'react';
import styled from '@emotion/styled';

const DataItem = props => {
    const Item = styled.div`
        text-decoration: underline
    `;
    return (
        <Item>
            {props.value}
        </Item>
    )
}

export default DataItem;