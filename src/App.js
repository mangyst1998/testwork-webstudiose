import './App.css';
import React,{useState} from 'react';
import styled from '@emotion/styled';
import data from './data';
import DataItem from './DataItem/DataItem';

function App() {
  const [inputValue, setInputValue] = useState('');
  const url = window.location.href;
  let value;

  const Results = styled.div`
    margin-top: 20px;
    font-size: 18px;
    background-color: rgb(209, 209, 209);
    text-align: left;
    padding: 10px 20px;
    height: 600px;
    overflow-y: auto;
  `;

  const EmptyState = styled.div`
    text-align: center;
    margin-top: 300px;
  `

  new Promise(resolve => {
    window.onload = () => {
      if( url !== 'http://localhost:3000/' ) {
        value = url.slice(22, url.length);
  
        while(value.includes('%20')){
          value = value.replace('%20', ' ');
        }
  
        setInputValue(value);
      }
    }
    resolve();
  }).then(() => {
    inputValue.length
    ? window.history.replaceState('', '', inputValue)
    : window.history.replaceState('', '', '/');
  }
  )

  const filtered = data.filter(item => item.toLowerCase().startsWith(inputValue));
  const result = filtered.map((item, index) => {
    return (
      <DataItem 
        key={index}
        value={item}
      />
    )
  })

  return (
    <div className="App">
      <input className="input" value={inputValue} onChange={(event) => setInputValue(event.target.value)} placeholder="Start typing"/>
      <Results> 
        {result.length 
          ? result 
          : <EmptyState>No data matched your input value</EmptyState>
        }
      </Results>
    </div>
  );
}

export default App;
